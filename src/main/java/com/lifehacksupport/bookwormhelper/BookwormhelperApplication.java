package com.lifehacksupport.bookwormhelper;

import com.lifehacksupport.bookwormhelper.booklistfetcher.GoodreadsOAuth;
import com.lifehacksupport.bookwormhelper.booklistfetcher.book.Book;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.List;


public class BookwormhelperApplication extends WebSecurityConfigurerAdapter {

    public final static Logger logger = Logger.getLogger(BookwormhelperApplication.class);

    public static void main(String[] args) {
        logger.setLevel(Level.WARN);
        GoodreadsOAuth.Initiate();
        List<Book> bookList = GoodreadsOAuth.getBookList();
        //bookList.get(1).getLibGenUrl("http://localhost:3000/books/search/");
        bookList.forEach(Book::download);
    }

}

