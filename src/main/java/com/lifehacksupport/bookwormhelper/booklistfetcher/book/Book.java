package com.lifehacksupport.bookwormhelper.booklistfetcher.book;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Book {
    final String NODE_API_URL = "http://localhost:3000/books/search/";
    final String BOOK_DOWNLOAD_LOCATION = "C:/Users/Reimo/Downloads/Books/";

    private String title, author, isbn10, isbn13;
    private List<HashMap<Object, Object>> searchResults;
    private ArrayList<URL> downloadUrls;


    public Book(String title, String author, String isbn10, String isbn13) {
        this.title = title;
        this.author = author;
        this.isbn10 = isbn10;
        this.isbn13 = isbn13;
        downloadUrls = new ArrayList<>();
        searchResults = new ArrayList<>();
    }

    private void searchFromLibGen(String query) {
        query = NODE_API_URL + query;
        //TODO 1. search by isbn
        //if results != null get ones with mobi, epub... pdf
        //TODO search by name
        //if results != null get ones with mobi, epub... pdf
        //return null
        try {
            System.out.println("Book: " + this.getTitle() + " \nISBN: " + this.getIsbn13());
            System.out.println("query: " + query);
            Document doc = Jsoup.connect(query).ignoreContentType(true).get();
            JSONArray resultArray = new JSONArray(doc.body().text());
            resultArray.forEach(result -> {
                var map = new HashMap<>();
                var jsonObj = new JSONObject(result.toString());
                System.out.println(result);
                map.put("Title", jsonObj.get("Title"));
                map.put("Author", jsonObj.get("Author"));
                map.put("Year", jsonObj.get("Year"));
                map.put("Extension", jsonObj.get("Extension"));
                map.put("DownloadPage", jsonObj.get("DownloadPage"));
                addSearchResult(map);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void populateDownloadUrls(String libGenUrl) {
        try {
            Document doc = Jsoup.connect(libGenUrl).get();
            Elements links = doc.body().getElementsByAttribute("title");
            links.forEach(link -> {
                if (link.parent().text().equals(link.attr("title"))) {
                    try {
                        addDownloadUrl(new URL(link.attr("href")));
                    } catch (MalformedURLException e) {
                        System.out.println("Malformed url: " + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("Exception in populateDownloadUrls: " + e.getMessage());
        }
    }

    private static void downloadUsingNIO(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
    }

    private URL getFileDownloadLink(URL downloadPageUrl) {
        try {
            Document doc = Jsoup.connect(downloadPageUrl.toString()).get();
            Elements links = doc.body().getElementsByAttribute("href");
            for (Element link : links) {
                if (link.text().equals("GET")) {
                    return new URL(link.attr("href"));
                }
            }
        } catch (IOException e) {
            System.out.println("Exception in getFileDownloadLink: " + e.getMessage());
        }
        return null;
    }

    public void download() {
        searchFromLibGen(this.getIsbn13());
        var libGenUrl = getSearchResults().get(0).get("DownloadPage").toString();
        populateDownloadUrls(libGenUrl);
        var fileURL = getFileDownloadLink(getDownloadUrls().get(0));
        var bookTitleWithSpacesRemoved = this.getTitle().trim().replaceAll("[\\\\/:*?\"<>|]", "").replace(" ", "_");
        var extension = this.getSearchResults().get(0).get("Extension").toString();
        var filename = bookTitleWithSpacesRemoved + "." + extension;
        var fileFullPath = BOOK_DOWNLOAD_LOCATION + filename;
        try {
            downloadUsingNIO(fileURL.toString(), fileFullPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn10() {
        return isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    private void addDownloadUrl(URL downloadUrl) {
        downloadUrls.add(downloadUrl);
    }

    private List<URL> getDownloadUrls() {
        return downloadUrls;
    }

    @Override
    public String toString() {
        return "TITLE: " + getTitle() + "; AUTHOR: " + getAuthor() + "; ISBN: " + getIsbn10() + "; ISBN13: " + getIsbn13();
    }

    public List<HashMap<Object, Object>> getSearchResults() {
        return searchResults;
    }

    public void addSearchResult(HashMap<Object, Object> searchResult) {
        this.searchResults.add(searchResult);
    }
}
