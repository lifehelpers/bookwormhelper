package com.lifehacksupport.bookwormhelper.booklistfetcher;

import com.google.api.client.auth.oauth.*;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.lifehacksupport.bookwormhelper.BookwormhelperApplication;
import com.lifehacksupport.bookwormhelper.booklistfetcher.book.Book;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;

public class GoodreadsOAuth {

    private static final String BASE_GOODREADS_URL = "https://www.goodreads.com";
    private static final String TOKEN_SERVER_URL = BASE_GOODREADS_URL + "/oauth/request_token";
    private static final String AUTHENTICATE_URL = BASE_GOODREADS_URL + "/oauth/authorize";
    private static final String ACCESS_TOKEN_URL = BASE_GOODREADS_URL + "/oauth/access_token";

    private static final String GOODREADS_KEY = "L1f3ALWjBEXk5TSUUUphPA";
    private static final String GOODREADS_SECRET = "0PzrukAI9afdgzpIskdSCnOHsQxU0V1tQT7R9yrKLg";

    private static OAuthParameters oAuthParameters;
    private static Logger logger = BookwormhelperApplication.logger;
    private static HttpRequestFactory requestFactory;

    public static void Initiate() {
        try {
            var signer = new OAuthHmacSigner();
            // Get Temporary Token
            var getTemporaryToken = new OAuthGetTemporaryToken(TOKEN_SERVER_URL);
            signer.clientSharedSecret = GOODREADS_SECRET;
            getTemporaryToken.signer = signer;
            getTemporaryToken.consumerKey = GOODREADS_KEY;
            getTemporaryToken.transport = new NetHttpTransport();
            var temporaryTokenResponse = getTemporaryToken.execute();

            // Build Authenticate URL
            var accessTempToken = new OAuthAuthorizeTemporaryTokenUrl(AUTHENTICATE_URL);
            accessTempToken.temporaryToken = temporaryTokenResponse.token;
            var authUrl = accessTempToken.build();

            // Redirect to Authenticate URL in order to get Verifier Code
            System.out.println("Goodreads oAuth sample: Please visit the following URL to authorize:");
            System.out.println(authUrl);
            logger.info("Waiting 5s to allow time for visiting auth URL and authorizing...");
            Thread.sleep(5000);
            logger.info("Waiting time complete - assuming access granted and attempting to get access token");

            // Get Access Token using Temporary token and Verifier Code
            var getAccessToken = new OAuthGetAccessToken(ACCESS_TOKEN_URL);
            getAccessToken.signer = signer;
            signer.tokenSharedSecret = temporaryTokenResponse.tokenSecret;
            getAccessToken.temporaryToken = temporaryTokenResponse.token;
            getAccessToken.transport = new NetHttpTransport();
            getAccessToken.consumerKey = GOODREADS_KEY;
            var accessTokenResponse = getAccessToken.execute();

            // Build OAuthParameters in order to use them while accessing the resource
            var oAuthParameters = new OAuthParameters();
            signer.tokenSharedSecret = accessTokenResponse.tokenSecret;
            oAuthParameters.signer = signer;
            oAuthParameters.consumerKey = GOODREADS_KEY;
            oAuthParameters.token = accessTokenResponse.token;
            GoodreadsOAuth.oAuthParameters = oAuthParameters;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    public static ArrayList<Book> getBookList() {
        var bookList = new ArrayList<Book>();
        try {
            requestFactory = new ApacheHttpTransport().createRequestFactory(GoodreadsOAuth.oAuthParameters);
            var user_id = getUserObject().getJSONObject("GoodreadsResponse").getJSONObject("user").get("id").toString();
            var bookListUrl = new GenericUrl("https://www.goodreads.com/review/list/" + user_id + ".xml?key="
                    + GOODREADS_KEY + "&v=2&shelf=to-read&per_page=200&page=1" + "&user_id=" + user_id + "&page=1");
            var shelfListResponse = requestFactory.buildGetRequest(bookListUrl).execute();
            var shelfListResponseJSON = XML.toJSONObject(shelfListResponse.parseAsString());
            var userBookList = shelfListResponseJSON.getJSONObject("GoodreadsResponse").getJSONObject("reviews")
                    .getJSONArray("review");
            for (int i = 0; i < userBookList.length(); i++) {
                var book = userBookList.getJSONObject(i).getJSONObject("book");
                var title = String.valueOf(book.get("title"));
                var author = String.valueOf(book.getJSONObject("authors").getJSONObject("author").get("name"));
                var isbn = String.valueOf(book.get("isbn"));
                var isbn13 = String.valueOf(book.get("isbn13"));
                bookList.add(new Book(title, author, isbn, isbn13));
            }
            return bookList;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    private static JSONObject getUserObject() {
        try {
            // Use OAuthParameters to access the desired Resource URL
            requestFactory = new ApacheHttpTransport().createRequestFactory(GoodreadsOAuth.oAuthParameters);
            var authUserUrl = new GenericUrl("https://www.goodreads.com/api/auth_user");
            var authUserResponse = requestFactory.buildGetRequest(authUserUrl).execute();
            return XML.toJSONObject(authUserResponse.parseAsString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    private static JSONArray getUserShelvesArray() {
        try {
            requestFactory = new ApacheHttpTransport().createRequestFactory(GoodreadsOAuth.oAuthParameters);
            var user_id = getUserObject().getJSONObject("GoodreadsResponse").getJSONObject("user").get("id").toString();
            var shelfListUrl = new GenericUrl("https://www.goodreads.com/shelf/list.xml?key="
                    + GOODREADS_KEY + "&user_id=" + user_id + "&page=1");

            var shelfListResponse = requestFactory.buildGetRequest(shelfListUrl).execute();
            var shelfListResponseJSON = XML.toJSONObject(shelfListResponse.parseAsString());
            var userShelves = shelfListResponseJSON.getJSONObject("GoodreadsResponse").getJSONObject("shelves")
                    .getJSONArray("user_shelf");
            for (int i = 0; i < userShelves.length(); i++) {
                var shelfName = userShelves.getJSONObject(i).get("name").toString();
                var bookCount = Integer.parseInt(userShelves.getJSONObject(i).getJSONObject("book_count").get("content").toString());
                logger.info("Shelf name: " + shelfName + ", " + bookCount + " books");
            }
            return userShelves;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}

