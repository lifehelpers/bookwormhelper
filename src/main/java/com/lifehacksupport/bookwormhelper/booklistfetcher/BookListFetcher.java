package com.lifehacksupport.bookwormhelper.booklistfetcher;

import com.lifehacksupport.bookwormhelper.BookwormhelperApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookListFetcher extends WebSecurityConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(BookwormhelperApplication.class, args);
    }

    @RequestMapping(value = "/ex/foos", method = RequestMethod.GET)
    @ResponseBody
    public String getFoosBySimplePath() {
        System.out.println("TEST");
        return "Get some Foos";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @ResponseBody
    public String getLogin() {
        System.out.println("TEST");
        return "Get some Foos";
    }



}
